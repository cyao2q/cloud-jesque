# cloud-jesque

#### 介绍
基于 https://github.com/gresrun/jesque  延时任务实现的支持cron表达式的调度示例

#### 软件架构
软件架构说明

spring-boot 2.0.3 
spring-cloud Finchley.SR3

#### 安装教程

```
ScheduleUtils.cronEnqueue(client, "scheduleService", "print", ScheduleUtils.createCronExpression(triggerTime));

ScheduleUtils.cronEnqueue(client, "scheduleService", "withParams", "0 0/1 * * * ?", 31L, "Cyao");

ScheduleUtils.delayedEnqueue(client, "scheduleService", "withParams", triggerTime, "你好啊", "世界");

```
