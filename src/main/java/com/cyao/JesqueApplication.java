package com.cyao;

import com.cyao.core.ScheduleUtils;
import net.greghaines.jesque.client.Client;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.Future;
import java.util.Date;

@RestController
@SpringBootApplication
@Validated
public class JesqueApplication {
    public static void main(String[] args) {
        SpringApplication.run(JesqueApplication.class, args);
    }

    @Resource
    private Client client;

    @GetMapping("/")
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("/add")
    public String add(@RequestParam @Future @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date triggerTime) {
        ScheduleUtils.cronEnqueue(client, "scheduleService", "print", ScheduleUtils.createCronExpression(triggerTime));

        ScheduleUtils.cronEnqueue(client, "scheduleService", "withParams", "0 0/1 * * * ?", 31L, "Cyao");

        ScheduleUtils.delayedEnqueue(client, "scheduleService", "withParams", triggerTime, "你好啊", "世界");

        return "success";
    }

    @GetMapping("/del")
    public String del() {
        ScheduleUtils.removeCronEnqueue(client, "scheduleService", "withParams", "0 0/1 * * * ?", 31L, "Cyao");
        return "success";
    }
}
