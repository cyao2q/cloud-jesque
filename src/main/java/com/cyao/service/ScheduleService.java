package com.cyao.service;

import com.cyao.core.ScheduleJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service("scheduleService")
public class ScheduleService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public void print() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logger.info("ScheduleServiceImpl.print任务执行中,当前时间>>" + sdf.format(new Date()));
    }

    public void withParams(ScheduleJob.ScheduleParams params) {
        logger.info("ScheduleServiceImpl.withParams任务执行中>>" + params);
    }
}
